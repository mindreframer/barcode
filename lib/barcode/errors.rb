class Barcode
  class ModelNotRegistered < StandardError
  end

  class ImproperModelBarcode < StandardError
  end
end