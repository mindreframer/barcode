RSpec.describe BarcodeSupervisorPartA do
  class CharFeeder
    attr_accessor :supervisor
    def initialize(supervisor)
      @supervisor = supervisor
    end

    def feed(str)
      str.each_char do |char|
        @supervisor.on_char(char)
      end
    end
  end

  it "correctly stores valid barcodes" do
    supervisor = BarcodeSupervisorPartA.new
    feeder = CharFeeder.new(supervisor)
    feeder.feed("*A*")
    expect(supervisor.barcode_history).to eql ["*A*"]
    feeder.feed("*A,") # should be dropped
    feeder.feed("*B*")
    expect(supervisor.barcode_history).to eql ["*A*", "*B*"]
  end

  it "correctly deal with chars in any combination" do
    supervisor = BarcodeSupervisorPartA.new
    feeder = CharFeeder.new(supervisor)
    feeder.feed("A") # drop
    expect(supervisor.barcode_history).to eql []
    feeder.feed("*A")
    feeder.feed("*") # complete first barcode
    expect(supervisor.barcode_history).to eql ["*A*"]
    feeder.feed("*B**C*") # 2 barcodes
    expect(supervisor.barcode_history).to eql ["*A*", "*B*", "*C*"]
  end
end


RSpec.describe BarcodeSupervisorPartB do
  class HumanCharFeeder
    attr_accessor :supervisor
    def initialize(supervisor)
      @supervisor = supervisor
    end

    def feed(str)
      str.each_char do |char|
        @supervisor.on_char(char)
        sleep 0.006
      end
    end
  end

  class GunCharFeeder
    attr_accessor :supervisor
    def initialize(supervisor)
      @supervisor = supervisor
    end

    def feed(str)
      str.each_char do |char|
        @supervisor.on_char(char)
        sleep 0.001
      end
    end
  end

  it "tells apart human vs machine input" do
    supervisor = BarcodeSupervisorPartB.new
    humanfeeder = HumanCharFeeder.new(supervisor)
    gunfeeder = GunCharFeeder.new(supervisor)

    gunfeeder.feed("*B**C*")
    expect(supervisor.barcode_history).to eql ["*B*", "*C*"]

    humanfeeder.feed("*HUMAN*") # should be ignored by the supervisor
    expect(supervisor.barcode_history).to eql ["*B*", "*C*"]

    gunfeeder.feed("*HUMAN*") # now lets pipe same input through the gunfeeder
    expect(supervisor.barcode_history).to eql ["*B*", "*C*", "*HUMAN*"]
  end
end
