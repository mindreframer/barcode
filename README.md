# Barcode

```
Premise
A barcode is an optical, machine-readable representation of data.

Code 39 is a common barcode symbology standard. The standard consists of 44 characters, which support the following:

  • uppercase letters: A through Z
  • digits: 0 through 9
  • special characters: $ / % + - .
  • empty space

an additional character, the * asterisk, is used as both a start and stop delimiter.

EXAMPLE
Barcode symbologies are often implemented as a font. For example, to encode “THIS-IS-A-BARCODE”, we would use a freely available Code 39 font to render a string as a barcode.

The following string:

*THIS-IS-A-BARCODE*

Would render:
￼

Problem
When a commodity USB barcode gun is plugged into a computer, the operating system recognizes it as a vanilla keyboard input device.

Let's design a barcode system for employees that have both a keyboard and barcode gun plugged into their workstation.


Phase I
Design a Barcode class that, provided an input string, validates whether it is a valid Code 39 Barcode. The input string should already include * start and stop delimiters.

📗JavaScript
class Barcode {
  constructor(input) {

  }

  isValid() {

  }
}
📕Ruby
class Barcode
  def initialize(input)

  end

  def valid?

  end
end


Phase II
We have an input device character stream. These characters could originate from keyboard, barcode gun, or both. The runtime environment is a web browser, so we have no access to low-level device hardware ID.

Design a BarcodeSupervisor class that:
Part A
  • given a stream of characters, is able to reason whether it has seen a barcode
  • upon detecting a string that looks like a barcode, pushes the string onto member array barcodeHistory
  • leverages the Barcode class authored in Phase I
Part B
  • distinguish between a barcode gun and ordinary keyboard usage with high certainty
  • only add a barcode to barcodeHistory if input originates from a barcode gun




📗JavaScript
class BarcodeSupervisor {
  constructor() {
    this.barcodeHistory = [];
    document.addEventListener('keypress', this.onChar.bind(this));
  }

  onChar(event) {
    const char = event.key; // example: 'a'
  }
}

📕Ruby
class BarcodeSupervisor
  def initialize
    @barcode_history = []
  end

  def on_char(c)

  end
end


Phase III
Invent a barcode format, based on Code 39, to uniquely identify records in our database.
  • Each barcode must resolve to a specific instance of a specific model
  • It should be simple and efficient to get the barcode for a given record
  • It should be simple and efficient to retrieve the record for a given barcode
  • There are many different models that will have barcodes, and it should be easy for developers to add new models to this scheme
  • Database records have a 32-bit integer ID that is unique per-model
  • Barcodes must be no longer than 25 characters (excluding the * delimiters)
PART A
Describe the barcode format in enough detail for a teammate to implement correctly. Feel free to write code if it helps you think through or explain the format, but this is not required.
PART B
Explain, at an architectural level, how you might implement this barcode system in code. Sketch out what classes, modules, and methods would be created, and how they would interact with existing code.



Phase IV
Consider that a model represented by the barcoding system is a dispensed prescription Rx bottle. The instance ID of this model is becoming numerically very large–too large (in trillions). On printed physical labels, the barcode to be represented is too long to fit on the Rx bottle stick-on labels.

Given the model encoding solution authored in Phase III, consider expanding the system's flexibility to support a “hexadecimal mode” that can optionally be engaged in an effort to reduce the number of characters necessary to identify a model. This mode must be backwards compatible with the original base-10 mode from Phase III.

```