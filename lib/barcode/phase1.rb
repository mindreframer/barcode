class Barcode
  attr_accessor :input
  def initialize(input)
    @input = input
  end

  def valid?
    check_asterisk? && valid_body? && min_length?
  end

  # * uppercase letters: A through Z
  # * digits: 0 through 9
  # * special characters: $ / % + - .
  # * empty space
  # negated valid chars regex, more info here:
  # - https://medium.com/rubyinside/the-new-absent-operator-in-ruby-s-regular-expressions-7c3ef6cd0b99
  INVALID_CHAR_REGEX = /(?~[A-Z0-9\$\/\%\+\-\.\ ])/
  VALID_BODY_REGEX = /^[A-Z0-9\$\/\%\+\-\.\ ]+$/
  def valid_body?
    return !! body.match(VALID_BODY_REGEX)
  end

  # surrounded by "*" chars
  def check_asterisk?
    return (input[0] == "*" && input[-1] == "*")
  end

  # min. 1 char body
  def min_length?
    body.length >= 1
  end

  def body
    input[1..-2]
  end
end
