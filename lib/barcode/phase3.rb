################################### REQUIREMENTS
# Phase III
# Invent a barcode format, based on Code 39, to uniquely identify records in our database.
#   • Each barcode must resolve to a specific instance of a specific model
#   • It should be simple and efficient to get the barcode for a given record
#   • It should be simple and efficient to retrieve the record for a given barcode
#   • There are many different models that will have barcodes, and it should be easy for developers to add new models to this scheme
#   • Database records have a 32-bit integer ID that is unique per-model
#   • Barcodes must be no longer than 25 characters (excluding the * delimiters)

# PART A
# Describe the barcode format in enough detail for a teammate to implement correctly.
# Feel free to write code if it helps you think through or explain the format, but this is not required.

# PART B
# Explain, at an architectural level, how you might implement this barcode system in code.
# Sketch out what classes, modules, and methods would be created, and how they would interact with existing code.

################################### PROPOSED SOLUTION

# PART A
# To accomodate the requirements for Phase III I would propose following barcode format:

# - max. 25 chars
# - max 10 chars to represent the ID (32-bit int -> max 2147483647)
# - 1 char separator between encoded model name and ID -> `/`
# - max 14 chars to represent the table / model name

# For the model name representation we need to maintain a registry to map longer table name to a shorter representation.
# The easiest solution would be to increment an index and map it to the original name, like:
# - table1 -> 0
# - table2 -> 1
# - t1 -> 2
# - any_name -> 3

# The simplicity of this approach is not without its downside:
# it is impossible to get any useful information just from the index alone without the models registry.
#
# As an alternative solution I would propose to keep most-possible information from the original model name and
# increment an index only within that particular model prefix.
# E.g for prefix length of 4 chars this would be:
# - model_a   -> mode0
# - model_b   -> mode1
# - model1234 -> mode2

# Let's make following assumptions for encoded model names:
# - from available 14 characters
#   - use 10 to keep the original model name
#   - use 4 to store the index within that prefix (up-to 9999)

# We can easily change this ratio to use more characters for the index part, yet current assumptions will probably
# be sufficient for most real-world applications.

# For model names shorter than max. prefix we can just keep the original name.

# This approach requires the model names to be append-only, to guarantee that we generate the same encoded name for
# a model name accross application restarts.

# The best way would be to initialize the models registry during application boot process
# with a predefined model names list and then lock it from any further modification at runtime.

# We would need 2 classes to model the requirements:

# - ModelsRegistry
#   - register(name) -> will store the model name and calculate the encoded / shorter name to be used in barcodes
#   - to_barcode(name) -> fetches the encoded name / raises on misses
#   - from_barcode(barcode) -> looks up the original model name from an encoded barcode name

# - ModelToBarcode
#   - will be initialized with a valid registry instance
#   - to_barcode(model, id) -> returns the barcode for this particular model / raises on misses
#   - from_barcode(barcode) -> returns [id, model] / raises on misses

# Existing code should use a populated ModelToBarcode instance by calling its public methods:
# - to_barcode(model, id)
# - from_barcode(barcode)

class Barcode::ModelsRegistry
  MAX_PREFIX = 10
  def initialize
    @prefix_map = {}

    # optimization for faster lookups
    @lookup = {}
    @rev_lookup = {}
  end

  def register(names)
    Array(names).map do |name|
      register_table(name)
    end.uniq
  end

  def to_barcode(name)
    (@lookup[name] or raise Barcode::ModelNotRegistered.new(name))
  end

  def from_barcode(barcode)
    (@rev_lookup[barcode] or raise Barcode::ModelNotRegistered.new(barcode))
  end

  private

  def register_table(name)
    name = name.downcase # normalize names!
    if name.size > MAX_PREFIX
      register_long_name(name)
    else
      register_short_name(name)
    end
    to_barcode(name)
  end

  def register_long_name(name)
    submap = prefix_map(name)
    return submap[name] if submap[name]

    # use count within current prefix as next index
    barcode = name_for_idx(name, submap.count)

    submap[name] = barcode
    @lookup[name] = barcode
    @rev_lookup[barcode] = name
  end

  def register_short_name(name)
    barcode = name.upcase
    @lookup[name] = barcode
    @rev_lookup[barcode] = name
  end

  def name_for_idx(name, idx)
    "#{submapkey(name)}#{idx}".upcase
  end

  def prefix_map(name)
    (@prefix_map[submapkey(name)] ||= {})
  end

  def submapkey(name)
    valid_chars_name = name.upcase.gsub(valid_regex, "")
    valid_chars_name[0..(MAX_PREFIX-1)]
  end

  def valid_regex
    Barcode::INVALID_CHAR_REGEX
  end
end

class Barcode::ModelToBarcode
  attr_accessor :registry
  def initialize(registry)
    @registry = registry
  end

  def to_barcode(model, id)
    "*#{registry.to_barcode(model)}/#{id}*"
  end

  def from_barcode(barcode)
    separator_idx = barcode.rindex("/") or raise Barcode::ImproperModelBarcode.new(barcode)
    # leave out the first / last chars (*)
    model_part = barcode[1..separator_idx-1]
    id_part  = barcode[separator_idx+1..-2]
    [registry.from_barcode(model_part), id_part.to_i]
  end
end
