RSpec.describe Barcode do
  it "correctly identifies valid input" do
    [
      "*A*",
      "*Z*",
      "*AZ*",
      "*0*",
      "*9*",
      "*$*",
      "*/*",
      "*%*",
      "*+*",
      "*-*",
      "*.*",
      "* *",
      "*ABCDEFG 1230 % + / % + *",
    ].each do |input|
      expect(Barcode.new(input)).to be_valid
    end
  end

  it "correctly identifies invalid input" do
    [
      "*A",
      "Z*",
      "*Z,*",
      "*a*"
    ].each do |input|
      expect(Barcode.new(input)).not_to be_valid
    end
  end
end
