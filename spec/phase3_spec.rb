RSpec.describe Barcode::ModelsRegistry do
  describe "register" do
    it "works for short model names" do
      registry = Barcode::ModelsRegistry.new
      expect(registry.register("table1")).to eql ["TABLE1"]
      expect(registry.register("table2")).to eql ["TABLE2"]
    end

    it "works for long model names" do
      registry = Barcode::ModelsRegistry.new
      models = [
        "tabletabletabletable1",
        "tabletabletabletable2",
        "tabletabletabletable1"
      ]
      expect(registry.register(models)).to eql ["TABLETABLE0", "TABLETABLE1"]
    end


    it "works for model names with invalid chars" do
      registry = Barcode::ModelsRegistry.new
      tables = [
        "table,ta_bletabletable1",
        "table!!tab/letab",
        "tabletabletabletable1"
      ]
      expect(registry.register(tables)).to eql ["TABLETABLE0", "TABLETAB/L0", "TABLETABLE1"]
      expect(registry.register("fun@ky&#ch()rs")).to eql ["FUNKYCHRS0"]

      expect(registry.from_barcode("FUNKYCHRS0")).to eql "fun@ky&#ch()rs"
    end
  end

  describe "from_barcode" do
    it "works for short table names" do
      registry = Barcode::ModelsRegistry.new

      tables = [
        "table1",
        "table2"
      ]
      registry.register(tables)
      expect(registry.from_barcode("TABLE1")).to eql "table1"
      expect(registry.from_barcode("TABLE2")).to eql "table2"
    end

    it "works for long table names" do
      registry = Barcode::ModelsRegistry.new
      tables = [
        "tabletabletabletable1",
        "tabletabletabletable2",
        "tabletabletabletable1"
      ]
      registry.register(tables)

      expect(registry.from_barcode("TABLETABLE0")).to eql "tabletabletabletable1"
      expect(registry.from_barcode("TABLETABLE1")).to eql "tabletabletabletable2"
    end

    it "raises for unregistered tables" do
      registry = Barcode::ModelsRegistry.new
      tables = [
        "tabletabletabletable1",
        "tabletabletabletable2",
        "tabletabletabletable1"
      ]
      registry.register(tables)

      expect(registry.from_barcode("TABLETABLE0")).to eql "tabletabletabletable1"

      expect {
        registry.from_barcode("TABLETABLE10")
      }.to raise_error(Barcode::ModelNotRegistered, "TABLETABLE10")
    end
  end

  describe "fullcheck" do
    it "corectly maps model names to encoded barcode and back" do
      registry = Barcode::ModelsRegistry.new
      name_pairs = [
        ["tabletabletabletable1", "TABLETABLE0"],
        ["tabletabletabletable2", "TABLETABLE1"],
        ["tabletabletable", "TABLETABLE2"],
        ["short1", "SHORT1"],
        ["short2", "SHORT2"],
      ]

      name_pairs.each do |pair|
        registry.register(pair[0])
        expect(registry.to_barcode(pair[0])).to eql pair[1]
        expect(registry.from_barcode(pair[1])).to eql pair[0]
      end
    end
  end
end


RSpec.describe Barcode::ModelToBarcode do
  describe "fullcheck" do
    it "corectly maps model names + ids to encoded barcode and back" do
      registry = Barcode::ModelsRegistry.new
      tables = [
        "tabletabletabletable1",
        "tabletabletabletable2",
        "tabletabletable",
        "short1",
        "short2",
      ]
      registry.register(tables)
      model_to_barcode = Barcode::ModelToBarcode.new(registry)

      to_barcode = ->(table, id) { model_to_barcode.to_barcode(table, id) }
      from_barcode = ->(code) { model_to_barcode.from_barcode(code) }
      expect(to_barcode.call("tabletabletabletable1", 4545)).to eql "*TABLETABLE0/4545*"
      expect(to_barcode.call("tabletabletabletable1", 2147483647)).to eql "*TABLETABLE0/2147483647*"
      expect(from_barcode.call("*TABLETABLE0/4545*")).to eql ["tabletabletabletable1", 4545]

      expect {
        from_barcode.call("*TABLETABLE10/45*")
      }.to raise_error(Barcode::ModelNotRegistered, "TABLETABLE10")

      expect {
        from_barcode.call("*TABLETABLE45*")
      }.to raise_error(Barcode::ImproperModelBarcode, "*TABLETABLE45*")

    end
  end
end
