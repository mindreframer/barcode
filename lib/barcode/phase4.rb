################################### REQUIREMENTS
# Phase IV
# Consider that a model represented by the barcoding system is a dispensed prescription Rx bottle.
# The instance ID of this model is becoming numerically very large–too large (in trillions).
# On printed physical labels, the barcode to be represented is too long to fit on the Rx bottle stick-on labels.

# Given the model encoding solution authored in Phase III,
# consider expanding the system's flexibility to support a “hexadecimal mode” that
# can optionally be engaged in an effort to reduce the number of characters necessary to identify a model.
# This mode must be backwards-compatible with the original base-10 mode from Phase III.


################################### PROPOSED SOLUTION

# To allow a hexadecimal mode, we could use the first char after the separator to potentially signal that
# the ID is hex-encoded.

# So we would go from largest model representation looking like:
# `*TABLETABLE9999/2147483647*` (max 32-bit int) to

# `*TABLETABLE9999/HFFFFFFFFF*` which would effectively translate to

# `*TABLETABLE9999/68719476735*`. Doing this we can store `68719476735 - 2147483647 => 66571993088` more IDs
# using the same 25 char limit for the barcodes.


class Barcode::ModelToBarcodeHexMode
  HEX_SEPARATOR = "/H"
  attr_accessor :registry
  def initialize(registry)
    @registry = registry
  end

  # Encode only to HEX representation
  def to_barcode(model, id)
    "*#{registry.to_barcode(model)}/H#{id.to_s(16)}*".upcase
  end

  # HEX decoding with fallback to base-10 decoding for backwards-compatibility
  def from_barcode(barcode)
    if barcode.rindex(HEX_SEPARATOR)
      from_base16_barcode(barcode)
    else
      from_base10_barcode(barcode)
    end
  end

  private

  def from_base16_barcode(barcode)
    separator_idx = barcode.rindex(HEX_SEPARATOR) or raise Barcode::ImproperModelBarcode.new(barcode)

    # leave out the first / last chars (*)
    model_part = barcode[1..separator_idx-1]

    # also ignore the H-char after separator
    id_part  = barcode[separator_idx+2..-2]
    [registry.from_barcode(model_part), id_part.hex]
  end

  def from_base10_barcode(barcode)
    separator_idx = barcode.rindex("/") or raise Barcode::ImproperModelBarcode.new(barcode)

    # leave out the first / last chars (*)
    model_part = barcode[1..separator_idx-1]
    id_part  = barcode[separator_idx+1..-2]
    [registry.from_barcode(model_part), id_part.to_i]
  end
end
