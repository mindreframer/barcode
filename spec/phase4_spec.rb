RSpec.describe Barcode::ModelToBarcodeHexMode do
  describe "fullcheck" do
    it "correctly encodes model names + id as hex and supports 10-based barcodes from phase III" do
      registry = Barcode::ModelsRegistry.new
      tables = [
        "tabletabletabletable1",
        "tabletabletabletable2",
        "tabletabletable",
        "short1",
        "short2",
      ]
      registry.register(tables)
      model_to_barcode = Barcode::ModelToBarcodeHexMode.new(registry)

      to_barcode = ->(table, id) { model_to_barcode.to_barcode(table, id) }
      from_barcode = ->(code) { model_to_barcode.from_barcode(code) }

      expect(to_barcode.call("tabletabletabletable1", 4545)).to eql "*TABLETABLE0/H11C1*"
      expect(to_barcode.call("tabletabletabletable1", 2147483647)).to eql "*TABLETABLE0/H7FFFFFFF*"

      # supports Phase III and Phase IV barcodes
      expect(from_barcode.call("*TABLETABLE0/4545*")).to eql ["tabletabletabletable1", 4545]
      expect(from_barcode.call("*TABLETABLE0/H11C1*")).to eql ["tabletabletabletable1", 4545]

      # max possible ID decoding
      expect(from_barcode.call("*TABLETABLE0/HFFFFFFFFF*")).to eql ["tabletabletabletable1", 68719476735]



    end
  end
end
