### Phase II
# We have an input device character stream.
# These characters could originate from keyboard, barcode gun, or both.
# The runtime environment is a web browser, so we have no access to low-level device hardware ID.
# Design a BarcodeSupervisor class that:


# Part A
#   • given a stream of characters, is able to reason whether it has seen a barcode
#   • upon detecting a string that looks like a barcode, pushes the string onto member array barcodeHistory
#   • leverages the Barcode class authored in Phase I
class BarcodeSupervisorPartA
  attr_accessor :acc
  attr_accessor :barcode_history
  def initialize
    @acc = ""
    @barcode_history = []
  end

  def on_char(c)
    @acc = @acc + c
    process_asterisk_char!(c) if c == "*" && acc.size > 1
  end

  private
  def process_asterisk_char!(c)
    acc_valid? ? process_complete_barcode! : reset_acc!
  end

  def acc_valid?
    Barcode.new(acc).valid?
  end

  def process_complete_barcode!
    @barcode_history << acc
    @acc = ""
  end

  def reset_acc!
    @acc = "*"
  end
end

# Part B
#   • distinguish between a barcode gun and ordinary keyboard usage with high certainty
#   • only add a barcode to barcodeHistory if input originates from a barcode gun
class BarcodeSupervisorPartB
  MAX_MACHINE_LATENCY = 0.005 # 5ms
  attr_accessor :acc
  attr_accessor :barcode_history
  def initialize
    @acc = ""
    @barcode_history = []
    @assumed_source = :gun
  end

  def on_char(c)
    @acc = @acc + c
    run_human_input_detection!
    process_asterisk_char!(c) if c == "*" && acc.size > 1
  end

  private
  def process_asterisk_char!(c)
    if acc_valid? && is_source_gun?
      process_complete_barcode!
    else
      reset_acc!
    end
  end

  def acc_valid?
    Barcode.new(acc).valid?
  end

  def is_source_gun?
    @assumed_source == :gun
  end

  def process_complete_barcode!
    @barcode_history << acc
    @assumed_source = :gun
  end

  def reset_acc!
    @acc = "*"
    @assumed_source = :gun
  end

  ## heuristic to tell machine vs. human input apart
  # we assume that high time distance between char inputs probably means human input
  def run_human_input_detection!
    @assumed_source = :human if is_human_input?
    @last_input_at = Time.now.to_f
  end

  def last_input_at
    @last_input_at ||= Time.now.to_f
  end

  def is_human_input?
    Time.now.to_f - last_input_at > MAX_MACHINE_LATENCY
  end
end
